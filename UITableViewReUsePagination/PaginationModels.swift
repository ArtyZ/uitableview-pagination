//
//  PaginationModels.swift
//  UITableViewReUsePagination
//
//  Created by Artur-Mac on 21/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import Foundation

struct Review: Decodable {
    let author: String
    let reviewText: String
    let score: Int
    let submitDate: String
    
    enum CodingKeys: String, CodingKey {
        case author, score, submitDate
        case reviewText = "review"
    }
}

struct PagingInfo: Decodable {
    let offset: Int
    let nextOffset: Int?
}

struct PaginationReviewsResponse: Decodable {
    let elements: [Review]
    let pagingInfo: PagingInfo
    let elementsCount: Int
    
    enum CodingKeys: String, CodingKey {
        case pagingInfo
        case elementsCount = "reviewsCount"
        case elements = "reviews"
    }
}
