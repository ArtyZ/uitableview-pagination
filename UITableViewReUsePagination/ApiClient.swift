//
//  ApiClient.swift
//  UITableViewReUsePagination
//
//  Created by Artur-Mac on 19/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import UIKit

enum ResultApi<Element> {
    case failure(Error)
    case success(Element)
}

protocol HasApiClient {
    var apiClient: ApiClient { get }
}

class ApiClient {
    static var baseURL: String = "https://meaqa.mocklab.io/product/p1144563101/reviews"
   
    func fetch(offset: Int, completion: @escaping((ResultApi<PaginationReviewsResponse>) -> Void)) {
        let url = "\(ApiClient.baseURL)?offset=\(offset)"
        let urlRequest = URLRequest(url: URL(string: url)!)
        
       URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            guard let httpUrlResponse = response as? HTTPURLResponse, httpUrlResponse.statusCode == 200, let data = data else {
                return completion(ResultApi.failure(ApiError.serverError))
            }

            guard let decodeData = try? JSONDecoder().decode(PaginationReviewsResponse.self, from: data) else {
                return completion(ResultApi.failure(ApiError.other(message: "JSON decoding failed")))
            }
            
            completion(ResultApi.success(decodeData))
            }.resume()
    }
}
