//
//  ViewController.swift
//  UITableViewReUsePagination
//
//  Created by Artur-Mac on 19/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ErrorPresenting, HasApiClient {
    let apiClient: ApiClient = ApiClient()
    
    private var viewModel: PaginationViewModel<Review>!
    
    @IBOutlet weak var tableView: UITableView!
    private let cellIdentifier = "list"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = PaginationViewModel<Review>(withServices: self)
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: .leastNormalMagnitude))
        
    }
}

extension ViewController: PaginationViewModelDelegate {
    func fetchReviewsCompleted(with newIndexPathsToReload: [IndexPath]?) {
        guard let newIndexToReload = newIndexPathsToReload else {
            return
        }
        
        tableView.performBatchUpdates({
            tableView.insertRows(at: newIndexToReload, with: .middle)
        }) { (finished) in
            if !finished {
                return
            }
            if let needMore = self.tableView.indexPathsForVisibleRows {
                let needMoreSorted = needMore.sorted()
                if needMoreSorted.last?.row == newIndexPathsToReload?.last?.row {
                    self.viewModel.loadMoreItems(offset: self.viewModel.nextOffset)
                }
            }
        }
    }
    
    func fetchReviewsFailed(with error: Error) {
        presentError(error)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return viewModel.footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return viewModel.heighForFooter(showFooterView: viewModel.hasMoreItemsToFetch)
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        viewModel.loadMoreItems(offset: viewModel.nextOffset)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.elements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell) ?? TableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        cell.label.text = viewModel.elements[indexPath.row].reviewText
        return cell
    }
}
