//
//  PaginationProtocol.swift
//  UITableViewReUsePagination
//
//  Created by Artur-Mac on 21/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import UIKit

protocol PaginationView {
     var footerView: UIView { get }
}

extension PaginationView {
    func setupFooter() {
        let indicatorView = UIActivityIndicatorView(style: .whiteLarge)
        indicatorView.color = .black
        indicatorView.hidesWhenStopped = true
        indicatorView.startAnimating()
        indicatorView.translatesAutoresizingMaskIntoConstraints = false
        footerView.addSubview(indicatorView)
        indicatorView.centerXAnchor.constraint(equalTo: footerView.centerXAnchor).isActive = true
        indicatorView.centerYAnchor.constraint(equalTo: footerView.centerYAnchor).isActive = true
    }
    
    func heighForFooter( showFooterView: Bool ) -> CGFloat {
        if !showFooterView {
            footerView.isHidden = true
            return .leastNonzeroMagnitude
        }
        footerView.isHidden = false
        return 50.0
    }
}

protocol PaginationProtocol {
    var hasMoreItemsToFetch: Bool { get }
    var isFetching: Bool { get }
    
    func fechingFromApi(index: Int)
}

extension PaginationProtocol {
    func loadMoreItems(offset: Int) {
        if !isFetching, hasMoreItemsToFetch {
            fechingFromApi(index: offset)
        }
    }
}
