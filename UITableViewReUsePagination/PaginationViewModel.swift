//
//  PaginationViewModel.swift
//  UITableViewReUsePagination
//
//  Created by Artur-Mac on 30/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import UIKit

protocol PaginationViewModelDelegate: class {
    func fetchReviewsCompleted(with newIndexPathsToReload: [IndexPath]?)
    func fetchReviewsFailed(with error: Error)
}

class PaginationViewModel<Element>: PaginationProtocol, PaginationView {
    var hasMoreItemsToFetch: Bool = true
    var isFetching: Bool = false
    var nextOffset: Int = 0
    var elements: [Element] = []
    let footerView = UIView()
    private weak var delegate: PaginationViewModelDelegate?
    private weak var apiClient: ApiClient?
    typealias Services = HasApiClient & PaginationViewModelDelegate

    init(withServices services: Services) {
        self.delegate = services
        apiClient = services.apiClient
        setupFooter()
    }
    
    func fechingFromApi(index: Int) {
        apiClient?.fetch(offset: index) { (result) in
            switch result {
            case.failure(let error):
                DispatchQueue.main.async {
                    self.delegate?.fetchReviewsFailed(with: error)
                }
            case.success(let paginationRespons):
                guard let newElements = paginationRespons.elements as? [Element] else {
                    return
                }
                self.elements.append(contentsOf: newElements)
                self.nextOffset = paginationRespons.pagingInfo.nextOffset ?? self.nextOffset
                if paginationRespons.pagingInfo.nextOffset == nil {
                    self.hasMoreItemsToFetch = false
                }
                
                DispatchQueue.main.async {
                    let indexPathsToReload = self.calculateIndexPathsToReload(from: newElements)
                    self.delegate?.fetchReviewsCompleted(with: indexPathsToReload)
                }
            }
        }
    }
    
    private func calculateIndexPathsToReload(from newReviews: [Element]) -> [IndexPath] {
        let startIndex = elements.count - newReviews.count
        let endIndex = startIndex + newReviews.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
}
